#include <iostream>
#include "Helper.h"
#include <Windows.h>
#include <string>
#include <filesystem>
#include <direct.h>
#include <errno.h>
#include <fstream> 
#include <cstdlib>
#include <vector>
#include <sys/types.h>
#include <tchar.h> 
#include <strsafe.h>
#pragma comment(lib, "User32.lib")


using namespace std;



string GetDir(void);
void CD(string path);
void ls(string file);


int main(void) 
{
	string str = "";
	string path = "";
	string file = "";
	vector<string> f;

	while (true)
	{
		getline(cin, str);
		Helper::trim(str);

		if (str.compare("pwd") == 0)
		{
			str = GetDir();
			cout << str;

		}

		else if (str.compare("cd") == 0)
		{
			cout << "Enter path name:";
			getline(cin, path);
			CD(path);
		}


		else if (str.compare("create") == 0)
		{
			cout << "Enter file name:";
			getline(cin, file);
			ofstream outfile(file);
		}


		else if (str.compare("ls") == 0)
		{
			str = GetDir();
			ls(str);
		}
		
	}


}


string GetDir()
{
	char buff[FILENAME_MAX];
	_getcwd(buff, FILENAME_MAX);
	std::string curr_dir(buff);
	return curr_dir;
}


void CD(string path)
{
	_chdir(path.c_str());

}



void ls(string file)
{
	char a = '*';

	file += a;

	WIN32_FIND_DATA data;

	HANDLE hFind = FindFirstFile(file.c_str(), &data);  // FILES
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			std::cout << data.cFileName << std::endl;
		} while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}

}


